# Русский
Калькулятор запускается из консоли. Для работы ему нужно передать параметры из раздела "Основные команды" и (если нужно) дополнительные аргументы.

## Основные команды
Введите **адрес** и **маску** интернет протокола, разделив их знаком пробела [ ] или дроби [/].
- Адрес можно ввести в десятичном и двоичном формате: [192.168.0.1] или [11000000.10101000.00000000.00000001].
- Маску можно ввести десятичном, двоичном форматах или в виде префикса: [255.255.255.0],  [11111111.11111111.11111111.00000000] или [24].
Адрес и маска всегда вводятся на первых двух позициях: сначала адрес, затем маска. Дальше могут присутствовать аргументы.

## Необязательные аргументы
Дополнительные (необязательные) аргументы вводятся по шаблону: [-и=з], где 'и' - имя аргумента, 'з' - его значение (если оно нужно).
- [-к] [-q] - выводит данные в кратком формате для дальнейшей программной обработки;
- [-р=2] [-s=2] - разбивает (если возможно) сеть на указанное  (здесь 2) количество подсетей (не работает, если использован аргумент [-т]/[-h]).
- [-т=3,2,4,3,2] [-h=3,2,4,3,2] - разбивает (если возможно) сеть на подсети, содержащие указанное количество терминалов. В данном случае на 2 сети по 2 терминала, 2 сети по 3 терминала и 1 сеть с 4 терминалами (не работает, если использован аргумент [-p]/[-s]).
- [-я=имя] [-l=имя] - выводит данные в локализации, которая указана в файле '%/localization/имя.lang' (убедитесь, что этот файл существует).

## Примеры команды:
- [192.168.1.1/24 -к -р=2] - выведет данные для введённой сети и 2 подсетей в кратком виде;
- [172.219.12.3 12 -р=3] - выведет данные для введённой сети и 3 подсетей в форматированном виде;
- [1.1.1.1/255.255.0.0] - выведет данные для введённой сети в форматированном виде.

## Другие команды:
- [?] [п] [h] - выводит эту справку по командам;
- [в] [v] - выводит информацию о версии программы и её авторе.
  
---
# English
Russian is selected by default. Use [-l=en] parameter for English.
The calculator is launched from the console. To work, it needs to pass parameters from the "Basic commands" section and (if necessary) additional arguments.

## Basic commands
Enter the Internet Protocol address and mask separated by space [ ] of backslash [/].
- you can enter address in decimal or binary format: [192.168.0.1] or [11000000.10101000.00000000.00000001].
- you can enter mask in decimal or binary format or as prefix: [255.255.255.0], [11111111.11111111.11111111.00000000], or [24].
Address and mask are expected to be in two first positions: address is dirst and mask is second. You can add some other parameters after them.

## Optional
Additional (optional) arguments are entered using [-n=v] patern where 'n' is a name of parameter and 'v` is its value (if it requires).
- [-к] [-q] - outputs data in a short format. For further software processing;
- [-р=2] [-s=2] - splits (if it's possible) the net into number of subnets specified in value (here - into 2 nets) (does not work if the [-т]/[-h] argument is used]).
- [-т=3,2,4,3,2] [-h=3,2,4,3,2] - Splits the network (if possible) into subnets containing the specified number of hosts. In this case splits into 2 networks with 2 hosts, 2 networks with 3 hosts and 1 network with 4 hosts (does not work if the [-p]/[-s] argument is used]).
- [-я=name] [-l=name] - outputs data in the locale specified in file '%/localization/nanem.lang' (make shure the file exists).

## Command examples:
- [192.168.1.1/24 -q -s=2] - displays data for the entered network and 2 subnets in a short form;
- [172.219.12.3 12 -s=3]  - outputs formatted data for the entered network and 3 subnets.
- [1.1.1.1/255.255.0.0]  - displays data for the entered network in formatted format.

## Other commands:
- [?] [п] [h] - displays help (this output);
- [в] [v]     - displays information about the program version and its author.