﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SimpleIpCalc
{
    /// <summary>Класс поддержки нескольких языков вывода информации</summary>
    internal class Localization
    {
        /// <summary>Словарь локализованных замен</summary>
        private Dictionary<string, string> Outputs { get; }
        
        /// <summary> Единственный конструктор создаёт словарь замен по выбранному языку</summary>
        /// <param name="languageName">Язык, на котором будет выполнен вывод информации</param>
        public Localization(string languageName)
        {
            Outputs = new Dictionary<string, string>();
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? string.Empty, "localization", languageName + ".lang");
            if (!File.Exists(path))
                throw new Exception($"Файл локализации [%/localization/{languageName}.lang] отсутствует в папке программы." +
                    $"\r\nThere is no [%/localization/{languageName}.lang] file in program folder.");
            var lines = File.ReadAllLines(path);

            string currentName = null;
            var currentString = new StringBuilder();
            foreach(var line in lines)
            {
                // строка с комментарием, если нужно
                if (line.StartsWith('#') || string.IsNullOrWhiteSpace(line))
                    continue;

                string value;
                if (line.StartsWith('.'))
                {
                    currentString.Append("\r\n");
                    value = line.Substring(1);
                }
                else
                {
                    value = line;
                }

                if (string.IsNullOrWhiteSpace(value)) 
                    continue;

                // название - на отдельной строке
                if (value.StartsWith('☺'))
                {
                    if (!string.IsNullOrWhiteSpace(currentName))
                        Outputs.Add(currentName, currentString.ToString());

                    currentName = value.Trim('☺', ' ', '\t');
                    currentString.Clear();
                    continue;
                }

                currentString.Append(value);
            }
            if (!string.IsNullOrWhiteSpace(currentName))
                Outputs.Add(currentName, currentString.ToString());
        }

        /// <summary>Выдаёт строку для вставки в вывод</summary>
        /// <param name="key">Название вставляемой строки</param>
        /// <returns>Строка для вставки в вывод на выбранном языке</returns>
        internal string Get(string key) => Outputs.ContainsKey(key) ? Outputs[key] : null;

    }
}
