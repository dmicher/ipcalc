﻿using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using SipCalc;

namespace SimpleIpCalc
{

    /// <summary> Точка входа </summary>
    public static class Program
    {
        /// <summary> Запускает основноую работу программы</summary>
        /// <param name="args">Аргументы командной строки</param>
        public static void Main(string[] args)
        {
            Quatronima ip;                  // введённый сетевой адрес
            Quatronima mask;                // введённая маска сети
            var decoration = true;          // флаг того, что вывод нужно красиво оформить
            uint subnetsCount = 0;          // количество подсетей, на которые нужно разбить сеть
            uint[] varLengthSubnets = null; // размеры (в терминалах) сетей переменной длины
            Localization @out;              // локализационная модель
            string subnetArgument = null;   // аргумент разбиения на подсети, принятый к исполнению

            // локализация
            var lang = args.FirstOrDefault(x => x.StartsWith("-я=") || x.StartsWith("-l="));
            lang = string.IsNullOrWhiteSpace(lang) 
                ? "ru" 
                : lang.Substring(lang.IndexOf('=') + 1);

            try
            {
                @out = new Localization(lang);
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка: не удалось прочитать файл локализации. Error: can't reed language file. " + e.Message);
                return;
            }

            // контроль и анализ входных аргументов
            if (args.Length < 1)
            {
                Console.WriteLine(@out.Get("Привет"));
                return;
            }

            // вспомогательные команды
            switch (args[0])
            {
                // справка по командам
                case "?":
                case "п":
                case "h":
                    Console.WriteLine(@out.Get("Справка"));
                    return;
                // справка по версии ПО и авторе
                case "в":
                case "v":
                    Console.WriteLine(@out.Get("Версия"));
                    return;
            }

            try
            {
                int argIndex;
                if (args[0].Contains('/'))
                {
                    argIndex = 1;
                    var parts = args[0].Split('/');
                    ip = new Quatronima(parts[0]);
                    mask = byte.TryParse(parts[1], out var b1)
                        ? new Quatronima(b1)
                        : new Quatronima(parts[1]);
                }
                else
                {
                    argIndex = 2;
                    ip = new Quatronima(args[0]);
                    mask = byte.TryParse(args[1], out var b)
                        ? new Quatronima(b)
                        : new Quatronima(args[1]);
                }

                if (!mask.IsMask)
                {
                    Console.WriteLine("Ошибка_1");
                    return;
                }

                if (args.Length > argIndex)
                {
                    while (argIndex < args.Length)
                    {
                        var argument = args[argIndex].Trim();

                        if (argument == "-q" || argument == "к")
                            decoration = false;

                        if (string.IsNullOrWhiteSpace(subnetArgument) &&
                            (argument.StartsWith("-p") || argument.StartsWith("-s")))
                        {
                            subnetArgument = argument;
                            var parts = argument.Split('=');
                            if (parts.Length != 2)
                            {
                                Console.WriteLine(@out.Get("Ошибка_2"));
                                return;
                            }

                            if (!uint.TryParse(parts[1], out var u))
                            {
                                Console.WriteLine(@out.Get("Ошибка_3"));
                                return;
                            }

                            subnetsCount = u;
                        }

                        if (string.IsNullOrWhiteSpace(subnetArgument) &&
                            (argument.StartsWith("-т") || argument.StartsWith("-h")))
                        {
                            subnetArgument = argument;
                            var parts = argument.Split('=');
                            if (parts.Length != 2)
                            {
                                Console.WriteLine(@out.Get("Ошибка_9"));
                                return;
                            }

                            parts = parts[1].Split(',');
                            if (parts.Length < 1)
                            {
                                Console.WriteLine(@out.Get("Ошибка_9"));
                                return;
                            }

                            varLengthSubnets = parts.Select(x => uint.TryParse(x, out var ui) ? ui : 0u).ToArray();
                            if (varLengthSubnets.Any(x => x == 0))
                            {
                                Console.WriteLine(@out.Get("Ошибка_9"));
                                return;
                            }
                        }

                        if (argument == "-к" || argument == "-q")
                            decoration = false;
                        argIndex++;
                    }
                }
            }
            catch (ArgumentException ae)
            {
                switch (ae.Message)
                {
                    case "Длина кватронимы":
                        Console.WriteLine(@out.Get("Ошибка_5"));
                        break;
                    case "Часть кватронимы не число":
                        Console.WriteLine(@out.Get("Ошибка_6"));
                        break;
                    case "Количество больше 32":
                        Console.WriteLine(@out.Get("Ошибка_7"));
                        break;
                    default:
                        Console.WriteLine(@out.Get("Ошибка_8"));
                        break;
                }

                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(@out.Get("Ошибка_4") + e.Message);
                return;
            }

            var mainNetwork = Calculator.GetSingleNetwork(ip, mask);
            var mainInfo = GetFullInfo(mainNetwork, decoration);

            if (decoration)
            {
                var header = mainInfo.NewRow();
                header["name"] = @out.Get("Основные_данные_категория");
                header["decimal"] = @out.Get("Основные_данные_10");
                header["byte"] = @out.Get("Основные_данные_2");
                header["hex"] = @out.Get("Основные_данные_16");

                foreach(DataRow row in mainInfo.Rows)
                {
                    switch (row["name"].ToString())
                    {
                        case "ip":
                            row["name"] = @out.Get("Основные_данные_имя_адрес");
                            break;
                        case "mask":
                            row["name"] = @out.Get("Основные_данные_имя_маска");
                            break;
                        case "wild":
                            row["name"] = @out.Get("Основные_данные_имя_обратная_маска");
                            break;
                        case "net_part":
                            row["name"] = @out.Get("Основные_данные_имя_сетевая_часть");
                            break;
                        case "host_part":
                            row["name"] = @out.Get("Основные_данные_имя_часть_терминала");
                            break;
                        case "broadcast":
                            row["name"] = @out.Get("Основные_данные_имя_широковещательный");
                            break;
                        case "host_first":
                            row["name"] = @out.Get("Основные_данные_имя_первый");
                            break;
                        case "host_last":
                            row["name"] = @out.Get("Основные_данные_имя_последний");
                            break;
                    }
                }

                Console.WriteLine(@out.Get("Основные_данные_заголовок") + ip + "/" + mask.ToPrefix());
                PrintTable(mainInfo, true, header);
            }
            else
            {
                PrintTable(mainInfo, false);
            }

            // подсети по заданному количеству
            if (subnetsCount > 0)
            {
                try
                {
                    var subnets = Calculator.SplitNetwork(mainNetwork, subnetsCount).ToArray();
                    if (subnets.Length < 1)
                    {
                        Console.WriteLine(@out.Get("Ошибка_10"));
                        return;
                    }

                    var table = GetSplitSubnetsInfo(subnets.ToArray());

                    if (decoration)
                    {
                        var header = table.NewRow();
                        header["number"] = @out.Get("Данные_подсетей_номер");
                        header["subnet"] = @out.Get("Данные_подсетей_адрес_подсети");
                        header["ipstart"] = @out.Get("Данные_подсетей_первый");
                        header["ipend"] = @out.Get("Данные_подсетей_последний");
                        header["broad"] = @out.Get("Данные_подсетей_широковещательный");
                        var top = @out.Get("Данные_подсетей_заголовок1");
                        var firstSubnet = subnets.First();
                        top = top
                            .Replace("{ipmask}", $"{mainNetwork} [{subnetArgument}]")
                            .Replace("{subnetMask}", firstSubnet.Mask.DecimalView + " = " + firstSubnet.MaskPrefix)
                            .Replace("{subnetWild}", firstSubnet.WildcardMask.DecimalView)
                            .Replace("{totalHosts}", (firstSubnet.HostsCount * subnets.Length).ToString())
                            .Replace("{subnets}", subnets.Length.ToString())
                            .Replace("{hostInSubnet}", firstSubnet.HostsCount.ToString());
                        Console.WriteLine(top);
                        PrintTable(table, true, header);
                        return;
                    }

                    Console.WriteLine();
                    PrintTable(table, false);
                    return;
                }
                catch (SipCalcException e)
                {
                    switch (e.Code)
                    {
                        case SipCalcExceptionCode.NoHostsInNetwork:
                            Console.WriteLine(@out.Get("Подсети_1"));
                            break;
                        case SipCalcExceptionCode.NoSubnetsRequired:
                            Console.WriteLine(@out.Get("Подсети_2") + " " + mainNetwork.HostsCount);
                            break;
                        case SipCalcExceptionCode.NotEnoughAddressesInNetwork:
                            Console.WriteLine(@out.Get("Подсети_2"));
                            Console.WriteLine(@out.Get("Подсети_3"));
                            break;
                        default:
                            Console.WriteLine(@out.Get("Ошибка_0") + " " + e.InnerException?.Message);
                            break;
                    }
                    return;
                }
            }
            
            // подсети по заданным терминалам
            if (varLengthSubnets != null && varLengthSubnets.Length > 0)
            {
                try
                {
                    var subnets = Calculator.GetSubnetsByHosts(mainNetwork, varLengthSubnets).ToArray();
                    if (subnets.Length < 1)
                    {
                        Console.WriteLine(@out.Get("Ошибка_10"));
                        return;
                    }

                    var table = GetHostSubnetsInfo(subnets);

                    if (decoration)
                    {
                        var header = table.NewRow();
                        header["number"] = @out.Get("Данные_подсетей_номер");
                        header["subnet"] = @out.Get("Данные_подсетей_адрес_маска_подсети");
                        header["ipstart"] = @out.Get("Данные_подсетей_первый");
                        header["ipend"] = @out.Get("Данные_подсетей_последний");
                        header["broad"] = @out.Get("Данные_подсетей_широковещательный");
                        header["hosts"] = @out.Get("Данные_подсетей_терминалы");
                        var top = @out.Get("Данные_подсетей_заголовок2");
                        var hostsBusy = subnets.Sum(x => x.HostsCount);
                        top = top
                            .Replace("{ipmask}", $"{mainNetwork} [{subnetArgument}]")
                            .Replace("{hostsBusy}", hostsBusy.ToString())
                            .Replace("{hostsFree}", (mainNetwork.AddressesCount - hostsBusy).ToString());
                        Console.WriteLine(top);
                        PrintTable(table, true, header);
                        return;
                    }

                    Console.WriteLine();
                    PrintTable(table, false);
                    return;

                }
                catch (SipCalcException e)
                {
                    switch (e.Code)
                    {
                        case SipCalcExceptionCode.NoHostsInNetwork:
                            Console.WriteLine(@out.Get("Подсети_1"));
                            break;
                        case SipCalcExceptionCode.NoSubnetsRequired:
                            Console.WriteLine(@out.Get("Подсети_2") + " " + mainNetwork.HostsCount);
                            break;
                        case SipCalcExceptionCode.NotEnoughAddressesInNetwork:
                            Console.WriteLine(@out.Get("Подсети_2"));
                            Console.WriteLine(@out.Get("Подсети_3"));
                            break;
                        default:
                            Console.WriteLine(@out.Get("Ошибка_0") + " " + e.InnerException?.Message);
                            break;
                    }
                    return;
                }
            }

            // здесь расчёт подсетей не требовался
            if (decoration) Console.WriteLine(@out.Get("Подсети_2") + " " + mainNetwork.HostsCount);
        }

        /// <summary>Подготавливает таблицу для вывода подробной инфомрации об указанной сети</summary>
        /// <param name="network">Сеть, по которой необходимо выдать информацию</param>
        /// <param name="splitBinaryView">Разбивать ли бинарное представление на сетевую и терминальную части</param>
        /// <returns>Структура с результатами обработки</returns>
        private static DataTable GetFullInfo(NetworkIPv4Info network, bool splitBinaryView = true)
        {
            var table = new DataTable("result") { Columns = { "name", "decimal", "byte", "hex" } };

            var delimiter = splitBinaryView
                ? (sbyte) (network.MaskPrefix ?? -1u)
                : (sbyte) -1;

            if (splitBinaryView && delimiter > 0 && delimiter < 32)
            {
                if (delimiter >= 24) delimiter += 3;
                else if (delimiter >= 16) delimiter += 2;
                else if (delimiter >= 8) ++delimiter;
            }

            string DelimiterBinaryView(string quatronimaBinaryView)
                => delimiter > 0
                    ? quatronimaBinaryView.Insert(delimiter, " ")
                    : quatronimaBinaryView;

            var row = table.NewRow();
            row["name"] = "ip";
            row["decimal"] = network.Address.DecimalView;
            row["byte"] = DelimiterBinaryView(network.Address.BinaryView);
            row["hex"] = network.Address.HexadecimalView;
            table.Rows.Add(row);

            row = table.NewRow();
            row["name"] = "mask";
            row["decimal"] = network.Mask + " = " + network.MaskPrefix;
            row["byte"] = DelimiterBinaryView(network.Mask.BinaryView);
            row["hex"] = network.Mask.HexadecimalView;
            table.Rows.Add(row);

            row = table.NewRow();
            row["name"] = "wild";
            row["decimal"] = network.WildcardMask.DecimalView;
            row["byte"] = DelimiterBinaryView(network.WildcardMask.BinaryView);
            row["hex"] = network.WildcardMask.HexadecimalView;
            table.Rows.Add(row);

            row = table.NewRow();
            row["name"] = "net_part";
            row["decimal"] = network.AddressNetPart.DecimalView;
            row["byte"] = DelimiterBinaryView(network.AddressNetPart.BinaryView);
            row["hex"] = network.AddressNetPart.HexadecimalView;
            table.Rows.Add(row);

            row = table.NewRow();
            row["name"] = "host_part";
            row["decimal"] = network.AddressHostPart.DecimalView;
            row["byte"] = DelimiterBinaryView(network.AddressHostPart.BinaryView);
            row["hex"] = network.AddressHostPart.HexadecimalView;
            table.Rows.Add(row);

            row = table.NewRow();
            row["name"] = "broadcast";
            row["decimal"] = network.BroadcastAddress.DecimalView;
            row["byte"] = DelimiterBinaryView(network.BroadcastAddress.BinaryView);
            row["hex"] = network.BroadcastAddress.HexadecimalView;
            table.Rows.Add(row);

            row = table.NewRow();
            row["name"] = "host_first";
            row["decimal"] = network.FirstHostAddress.DecimalView;
            row["byte"] = DelimiterBinaryView(network.FirstHostAddress.BinaryView);
            row["hex"] = network.FirstHostAddress.HexadecimalView;
            table.Rows.Add(row);

            row = table.NewRow();
            row["name"] = "host_last";
            row["decimal"] = network.LastHostAddress.DecimalView;
            row["byte"] = DelimiterBinaryView(network.LastHostAddress.BinaryView);
            row["hex"] = network.LastHostAddress.HexadecimalView;
            table.Rows.Add(row);

            return table;
        }

        /// <summary>Таблица для отображения подсетей при разбиении на равное количество подсетей</summary>
        /// <param name="networks">Массив подсетей</param>
        /// <returns>Таблица для отображения данных</returns>
        private static DataTable GetSplitSubnetsInfo(NetworkIPv4Info[] networks)
        {
            if (networks == null || networks.Length < 1)
                return null;

            var table = new DataTable { Columns = { "number", "ipstart", "ipend", "subnet", "broad" } };
            for(var i = 0; i < networks.Length; ++i)
            {
                var row = table.NewRow();
                row["number"] = i;
                row["subnet"] = networks[i].NetworkAddress.DecimalView;
                row["broad"] = networks[i].BroadcastAddress.DecimalView;
                row["ipstart"] = networks[i].FirstHostAddress.DecimalView;
                row["ipend"] = networks[i].LastHostAddress.DecimalView;
                table.Rows.Add(row);
            }

            return table;
        }

        /// <summary>Таблица для отображения подсетей при разбиении по массиву терминалов</summary>
        /// <param name="networks">Посчитанные подсети для отображения</param>
        /// <returns>Таблица данных</returns>
        private static DataTable GetHostSubnetsInfo(NetworkIPv4Info[] networks)
        {
            if (networks == null || networks.Length < 1)
                return null;

            var table = new DataTable("varLengthSubnets")
                { Columns = { "number", "subnet", "broad", "ipstart", "ipend", "hosts" } };

            for (var i = 0; i < networks.Length; ++i)
            {
                var row = table.NewRow();
                row["number"] = i;
                row["subnet"] = networks[i].NetworkAddress + "/" + networks[i].MaskPrefix;
                row["broad"] = networks[i].BroadcastAddress.DecimalView;
                row["ipstart"] = networks[i].FirstHostAddress.DecimalView;
                row["ipend"] = networks[i].LastHostAddress.DecimalView;
                row["hosts"] = networks[i].HostsCount.ToString();
                table.Rows.Add(row);
            }

            return table;
        }

        /// <summary> Красиво печатает таблицу данных в консоль</summary>
        /// <param name="table">Таблица с произвольными данными</param>
        /// <param name="firstRow">Строка "Заголовок" для таблицы</param>
        /// <param name="isDecorated">Выполнять ли декорирование таблицы данных</param>
        [SuppressMessage("ReSharper", "FormatStringProblem")]
        private static void PrintTable(DataTable table, bool isDecorated, DataRow firstRow = null)
        {
            var sizes = new int[table.Columns.Count];
            for (var i = 0; i < sizes.Length; ++i)
            {
                sizes[i] = table.Rows.Cast<DataRow>().Max(x => (x[i]?.ToString()?.Trim().Length) ?? 0);
                if (isDecorated && firstRow != null)
                {
                    var headerSize = firstRow[i]?.ToString()?.Length ?? 0;
                    if (sizes[i] < headerSize) sizes[i] = headerSize;
                }
            }

            int decorationSize = sizes.Sum() + sizes.Length * 3 - 1;

            if (isDecorated && firstRow != null)
            {
                Console.WriteLine("╔" + new string('═', decorationSize) + "╗");
                for (var i = 0; i < table.Columns.Count; ++i)
                    Console.Write("║ {0, -" + sizes[i] + "} ", firstRow[i].ToString()?.Trim());
                Console.Write("║" + "\r\n");
                Console.WriteLine("╙" + new string('─', decorationSize) + "╜");
            }

            foreach (DataRow row in table.Rows)
            {
                for (var i = 0; i < table.Columns.Count; ++i)
                    Console.Write((isDecorated ? "│ " : null) +
                                  "{0, -" + sizes[i] + "} ", row[i].ToString()?.Trim());
                Console.Write((isDecorated ? "│" : null) + "\r\n");
            }

            if (isDecorated) Console.WriteLine("└" + new string('─', decorationSize) + "┘");
        }
    }
}
