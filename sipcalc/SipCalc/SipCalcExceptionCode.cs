﻿namespace SipCalc
{
    /// <summary>Коды ошибок калькулятора</summary>
    public enum SipCalcExceptionCode
    {
        /// <summary>В разбиваемой сети нет свободных адресов</summary>
        NoHostsInNetwork,

        /// <summary>Сеть не нуждается в разбиении</summary>
        NoSubnetsRequired,

        /// <summary>В разбиваемой сети нет достаточного количества свободных адресов</summary>
        NotEnoughAddressesInNetwork,

        /// <summary>Не удаётся проинициализировать сеть</summary>
        CanNotInitializeNetwork
    }
}