﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SipCalc
{
    /// <summary>
    /// Кватронима - целое 32-разрядное число, которое можно представить в качестве 4 однобайтовых чисел.
    /// Кватронимой описывается адрес и маска интернет протокола версии 4.
    /// </summary>
    public class Quatronima : IEquatable<Quatronima>, IComparable
    {
        /// <summary> Внутреннее значение числа </summary>
        public uint Value { get; }

        /// <summary> Возвращает массив из 4 байтов, представляющих значение кватронимы</summary>
        private IEnumerable<byte> Bytes
        {
            get
            {
                var value = Value;
                var bytes = new byte[4];
                for (var i = 0; i < 4; ++i)
                {
                    bytes[3 - i] = (byte)value; // намерено усекает старшие биты
                    value >>= 8; // сдвигает биты для следующей итерации
                }
                return bytes;
            }
        }

        /// <summary> Формирует кватрониму по её заданному значению </summary>
        /// <param name="value">(опционально) Значение кватронимы в виде целого 32-разрядного положительного числа</param>
        public Quatronima(uint value = 0) => Value = value;

        /// <summary>Сформирует кватрониму из массива 4 байтов</summary>
        /// <param name="bytes">Массив байтов размером строго 4 элемента</param>
        /// <exception cref="ArgumentException"/>
        public Quatronima(byte[] bytes)
        {
            if (bytes.Length != 4)
                throw new ArgumentException("Invalid input array size. " +
                                            "Неверный размер входного массива.");
            for (var i = 0; i < 4; ++i) Value += (uint) bytes[i] << (3 - i) * 8;
        }

        /// <summary>
        /// Сформирует кватрониму из строки, переданной в формате десятичной записи [192.168.0.1] либо в 
        /// формате двоичной записи [10101010.10101010.10101010.10101010]
        /// </summary>
        /// <param name="input">Входная строка с записанной кватронимой</param>
        /// <exception cref="ArgumentException"/>
        public Quatronima(string input)
        {
            var parts = input.Split('.');
            if (parts.Length != 4)
                throw new ArgumentException("Invalid quatronima format. " +
                                            "Неверный формат кватронимы.");

            var isDecimal = input.Length < 33;

            for (var i = 0; i < 4; ++i)
                Value += (isDecimal
                    ? uint.TryParse(parts[i], out var number)
                        ? number
                        : throw new ArgumentException("Some part of quatronima is not a number. " +
                                                      "Одна из частей кватронимы - не число.")
                    : Convert.ToUInt32(parts[i], 2)) << (3 - i) * 8;
        }

        /// <summary>Сформирует кватрониму по количеству и виду единиц, указанных справа в маске </summary>
        /// <param name="prefix">Количество заполняемых знаков</param>
        /// <exception cref="ArgumentException"/>
        public Quatronima(byte prefix)
        {
            if (prefix > 32) throw new ArgumentException("Количество больше 32");
            Value = (uint) ((long) (1 << prefix) - 1) << 32 - prefix;
        }

        /// <summary>Выдаёт строковое представление кватронимы в формате по умолчанию</summary>
        /// <returns>Строка в представлении десятичных чисел</returns>
        public override string ToString() => DecimalView;

        /// <summary>Строковое представление в десятичной системе счисления</summary>
        public string DecimalView
        {
            get
            {
                var builder = new StringBuilder();
                var first = true;
                foreach (var b in Bytes)
                {
                    if (first) first = false;
                    else builder.Append(".");
                    builder.Append(b.ToString());
                }
                return builder.ToString();
            }
        }

        /// <summary>Строковое представление в шестнадцатиричной системе счисления</summary>
        public string HexadecimalView
        {
            get
            {
                var builder = new StringBuilder();
                var first = true;
                foreach (var b in Bytes)
                {
                    if (first) first = false;
                    else builder.Append(":");
                    var tmp = Convert.ToString(b, 16);
                    if (tmp.Length < 2)
                        tmp = "0" + tmp;
                    builder.Append(tmp);
                }
                return builder.ToString();
            }
        }

        /// <summary>Строковое представление в двоичной системе счисления</summary>
        public string BinaryView
        {
            get
            {
                var builder = new StringBuilder();
                var first = true;
                foreach (var b in Bytes)
                {
                    if (first) first = false;
                    else builder.Append(".");
                    var tmp = Convert.ToString(b, 2);
                    if (tmp.Length < 8)
                        tmp = new string('0', 8 - tmp.Length) + tmp;
                    builder.Append(tmp);
                }
                return builder.ToString();
            }
        }

        /// <summary> Считает количество количество знаков 1 или 0, идущих под ряд, со старшего или младшего битов</summary>
        /// <returns>Количество знаков согласно заданному правилу. Возвращает <see langword="null"/>, если <see langword="this"/> не маска</returns>
        public uint? ToPrefix()
        {
            if (!IsMask) return null;
            var tmp = Value - ((Value >> 1) & 0xdb6db6db) - ((Value >> 2) & 0x49249249);
            return ((tmp + (tmp >> 3)) & 0xc71c71c7) % 63;
        }

        /// <summary> Проверяет, может ли текущая кватронима быть маской сети</summary>
        /// <returns><see langword="true"/> - может быть маской; <see langword="false"/> - не может быть маской</returns>
        public bool IsMask
        {
            get
            {
                var wild = ~Value;
                return (wild & (wild + 1)) == 0;
            }
        }

        #region Операции с кватронимами

        /// <summary> Сложение кватронимы с целым числом. Переполнения не допускает.</summary>
        /// <param name="a">Кватронима</param>
        /// <param name="count">добавляемое целое число</param>
        /// <returns>Новая кватронима и изменённым <see cref="Value"/></returns>
        public static Quatronima operator +(Quatronima a, long count)
        {
            var newValue = a.Value + count;

            uint result;
            if (newValue > uint.MaxValue) result = uint.MaxValue;
            else if (newValue < uint.MinValue) result = uint.MinValue;
            else result = (uint)newValue;

            return new Quatronima(result);
        }

        /// <summary> Сложение целого числа с кватронимой. Переполнения не допускает.</summary>
        /// <param name="count">Число, к которому прибавляется кватронима</param>
        /// <param name="b">Кватронима</param>
        /// <returns>Новая кватронима и изменённым <see cref="Value"/></returns>
        public static Quatronima operator +(long count, Quatronima b) => b + count;

        /// <summary>Сложвение двух кватроним. Переполнения не допускает. </summary>
        /// <param name="a">Первое слогаемое</param>
        /// <param name="b">Второе слогаемое</param>
        /// <returns>Новая кватронима и изменённым <see cref="Value"/></returns>
        public static Quatronima operator +(Quatronima a, Quatronima b) => a + b.Value;

        /// <summary>Вычетание из кватронимы целого положительного числа. Переполнения не допускает</summary>
        /// <param name="a">Кватронима</param>
        /// <param name="count">Вычитаемое число</param>
        /// <returns>Новая кватронима и изменённым <see cref="Value"/></returns>
        public static Quatronima operator -(Quatronima a, uint count) => a + (-count);

        /// <summary>Разница между кватронимами. Переполнения не допускает</summary>
        /// <param name="a">Кватронима, из которой вычитается</param>
        /// <param name="b">Кватронима, которая вычетается</param>
        /// <returns>Новая кватронима и изменённым <see cref="Value"/></returns>
        public static Quatronima operator -(Quatronima a, Quatronima b) => a - b.Value;

        /// <summary>Вычитание из целого числа кватронимы</summary>
        /// <param name="count">Число, из которого вычета естся кватронима</param>
        /// <param name="b">Вычетаемая кватронима</param>
        /// <returns>Новая кватронима и изменённым <see cref="Value"/></returns>
        public static Quatronima operator -(uint count, Quatronima b) => new Quatronima(count) - b;

        /// <summary>Возвращает обратную кватрониму (в которой все 1 заменены 0 и наоборот)</summary>
        /// <returns>Обратная кватронима</returns>
        public static Quatronima operator ~(Quatronima a) => new Quatronima(~a.Value);

        /// <summary> Побитово перемножает две кватронимы между собой (порядок значения не имеет) </summary>
        /// <param name="a">Кватронима А</param>
        /// <param name="b">Кватронима Б</param>
        /// <returns>Новая кватронима с применённым умножением</returns>
        public static Quatronima operator &(Quatronima a, Quatronima b)
            => new Quatronima(a.Value & b.Value);

        /// <summary> Проверяет равенство между кватронимами</summary>
        /// <param name="a">левая кватронима</param>
        /// <param name="b">правая кватронима</param>
        /// <returns><see langword="true"/> если равны</returns>
        public static bool operator ==(Quatronima a, Quatronima b)
        {
            if (a is null) return b is null;
            return a.Equals(b);
        }

        /// <summary>Проверяет неравенствор между кватронимами</summary>
        /// <param name="a">левая кватронима</param>
        /// <param name="b">правая кватронима</param>
        /// <returns><see langword="true"/> если не равны</returns>
        public static bool operator !=(Quatronima a, Quatronima b) => !(a == b);

        /// <summary>Проверяет утверждение, что левая кватронима меньше правой</summary>
        /// <param name="a">левая кватронима</param>
        /// <param name="b">правая кватронима</param>
        /// <returns><see langword="true"/> если меньше</returns>
        public static bool operator <(Quatronima a, Quatronima b) => a.Value < b.Value;

        /// <summary>Проверяет утверждение, что левая кватронима больше правой</summary>
        /// <param name="a">левая кватронима</param>
        /// <param name="b">правая кватронима</param>
        /// <returns><see langword="true"/> если больше</returns>
        public static bool operator >(Quatronima a, Quatronima b) => a.Value > b.Value;

        /// <summary>Проверяет утверждение, что левая кватронима меньше правой или равна ей</summary>
        /// <param name="a">левая кватронима</param>
        /// <param name="b">правая кватронима</param>
        /// <returns><see langword="true"/> если меньше или равна</returns>
        public static bool operator <=(Quatronima a, Quatronima b) => a == b || a < b;

        /// <summary>Проверяет утверждение, что левая кватронима больше правой или равна ей</summary>
        /// <param name="a">левая кватронима</param>
        /// <param name="b">правая кватронима</param>
        /// <returns><see langword="true"/> если больше или равна</returns>
        public static bool operator >=(Quatronima a, Quatronima b) => a == b || a > b;

        #endregion

        #region Реализация инерфейсов

        /// <summary> Проверяет равенство двех кватроним между собой </summary>
        /// <param name="other">Вторая кватронима</param>
        /// <returns>Результат сравнения по их значениям </returns>
        public bool Equals(Quatronima other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Value == other.Value;
        }

        /// <summary>Сравнивает кватрониму с любым иным объектом</summary>
        /// <param name="obj">Любой объект</param>
        /// <returns>Результат сравнения</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((Quatronima)obj);
        }

        /// <summary>Получает хэш код кватронимы</summary>
        /// <returns>Обычный хэш код значения</returns>
        public override int GetHashCode() => (int)Value;

        /// <summary>Сравнивает кватронимы между собой и другими объектами</summary>
        /// <param name="obj">Любой иной объект</param>
        /// <returns>-1 = текущая кватронима меньше, 0 равна, +1 больше объекта сравнения</returns>
        public int CompareTo(object obj)
        {
            if (obj is Quatronima other) return this.Value.CompareTo(other.Value);
            return -1;
        }

        #endregion

    }
}
