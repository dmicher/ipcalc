﻿using System;

namespace SipCalc
{
    /// <summary> Класс, обозначающий сеть или подсеть</summary>
    public class NetworkIPv4Info
    {
        /// <summary>Объявляет класс сети. Требует адрес и маску</summary>
        /// <param name="address">Произвольный адрес в сети</param>
        /// <param name="mask">Маска сети</param>
        /// <exception cref="ArgumentException"/>
        public NetworkIPv4Info(Quatronima address, Quatronima mask)
        {
            Address = address;
            if (!mask.IsMask)
                throw new ArgumentException("Mask parameter does not meet the requirements " +
                                            "for Internet Protocol mask.", nameof(mask));
            Mask = mask;
        }

        /// <summary>Указанный адрес в сети</summary>
        public Quatronima Address { get; }

        /// <summary>Маска сети</summary>
        public Quatronima Mask { get; }

        /// <summary>Сетевая часть указанного адреса в сети</summary>
        public Quatronima AddressNetPart => NetworkAddress;

        /// <summary>Терминальная часть указанного адреса в сети</summary>
        public Quatronima AddressHostPart => Address & WildcardMask;

        /// <summary>Маска сети в виде префикса</summary>
        public uint? MaskPrefix => Mask.ToPrefix();

        /// <summary>Обратная маска сети</summary>
        public Quatronima WildcardMask => ~Mask;

        /// <summary>Адрес сети</summary>
        public Quatronima NetworkAddress => Address & Mask;

        /// <summary>Первый доступный адрес для терминала в сети</summary>
        public Quatronima FirstHostAddress => NetworkAddress + 1;

        /// <summary>Широковещательный адрес</summary>
        public Quatronima BroadcastAddress => NetworkAddress + WildcardMask;

        /// <summary>Последний доступный адрес для терминала в сети</summary>
        public Quatronima LastHostAddress => BroadcastAddress - 1;

        /// <summary>Общее количество адресов в сети</summary>
        public uint AddressesCount => (BroadcastAddress & WildcardMask).Value + 1;

        /// <summary>Количество адресов в сети, доступных для терминалов</summary>
        public uint HostsCount => (BroadcastAddress & WildcardMask).Value - 1;

        /// <summary>Строковое представление сети</summary>
        /// <returns>Возвращает краткую запись адреса сети и её маски</returns>
        public override string ToString() => NetworkAddress.DecimalView + "/" + MaskPrefix;
    }
}