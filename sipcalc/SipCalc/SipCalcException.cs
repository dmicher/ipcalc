﻿using System;

namespace SipCalc
{
    /// <summary>Исключение, выбрасываемое калькулятором</summary>
    public class SipCalcException : Exception
    {
        /// <summary>Допустимый код ошибки</summary>
        public SipCalcExceptionCode Code { get; }

        /// <summary>Создаёт исключение</summary>
        /// <param name="code">Код ошибки</param>
        /// <param name="innerException">(опционально) Внутреннее исключение, если нужно</param>
        public SipCalcException(SipCalcExceptionCode code, Exception innerException = null)
            : base(code.ToString(), innerException)
            => Code = code;
    }
}