﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SipCalc
{
    /// <summary> Основной модуль расчётов </summary>
    public static class Calculator
    {
        /// <summary>Возвращает информацию о сети по введённым </summary>
        /// <param name="address">Произвольный адрес из сети</param>
        /// <param name="mask">Маска сети</param>
        /// <returns>Объект информации по сети</returns>
        /// <exception cref="SipCalcException"/>
        public static NetworkIPv4Info GetSingleNetwork(Quatronima address, Quatronima mask)
        {
            try
            {
                return new NetworkIPv4Info(address, mask);
            }
            catch (Exception e)
            {
                throw new SipCalcException(SipCalcExceptionCode.CanNotInitializeNetwork, e);
            }
        }

        /// <summary>Разбивает указанную сеть на необходимое количество подсетей</summary>
        /// <param name="network">Делимая сеть</param>
        /// <param name="subnetsRequired">Необходимое количество подсетей</param>
        /// <exception cref="SipCalcException"/>
        /// <returns>Перечисление подсетей, на который сеть была разделена</returns>
        public static IEnumerable<NetworkIPv4Info> SplitNetwork(NetworkIPv4Info network, uint subnetsRequired)
        {
            if (network.HostsCount < 1)
                throw new SipCalcException(SipCalcExceptionCode.NoHostsInNetwork);

            if (subnetsRequired == 0)
                throw new SipCalcException(SipCalcExceptionCode.NoSubnetsRequired);

            var bytesCount = 0; // количество байтов, достаточное для размещения нужного количества подсетей
            int ipIntervals;
            do
            {
                ipIntervals = 1 << ++bytesCount;
            } while (ipIntervals < subnetsRequired);

            if (network.AddressesCount <= (1 << bytesCount) * 2) // количество широковещательных адресов и адресов подсетей
                throw new SipCalcException(SipCalcExceptionCode.NotEnoughAddressesInNetwork);

            var subnetMaskPrefix = (byte) ((network.MaskPrefix ?? 0) + bytesCount);
            var subnetsMask = new Quatronima(subnetMaskPrefix);
            var subnetBytePosition = 32 - subnetMaskPrefix; // младший байт маски подсетей (здесь переключаются подсети)

            var subnets = new List<NetworkIPv4Info>();
            for (var i = 0; i < ipIntervals; ++i)
                subnets.Add(new NetworkIPv4Info(network.NetworkAddress + (i << subnetBytePosition), subnetsMask));
            return subnets;
        }

        /// <summary>
        /// Разбивает указанную сеть на подсети переменной длины таким образом, чтобы в эти подсети
        /// можно было разместить (минимально достаточным образом) все перечисленные терминалы из
        /// указанного массива.
        /// </summary>
        /// <param name="network">Разбиваемая сеть</param>
        /// <param name="hosts">
        /// Массив терминалов, где каждый отдельный элемент массива - подсеть, а его значение - количество терминалов.
        /// </param>
        /// <returns>Перечисление подсетей (результат разбиения)</returns>
        public static IEnumerable<NetworkIPv4Info> GetSubnetsByHosts(NetworkIPv4Info network, uint[] hosts)
        {
            if (network.HostsCount < 1)
                throw new SipCalcException(SipCalcExceptionCode.NoHostsInNetwork);

            if (hosts == null || hosts.Length < 1)
                throw new SipCalcException(SipCalcExceptionCode.NoSubnetsRequired);
            
            // hosts 2,7,3,2,2,7 => hosts 4,16,8,4,4,16 (ближайший больший порядок бинарного числа)
            // [исходя из: количество хостов = размер сети - 2 (сеть и широковещат.)]
            var subnetsArr = new uint[hosts.Length];
            for (var i = 0; i < hosts.Length; ++i)
            {
                var j = 0;
                long value;
                do
                {
                    value = (long)2 << j++;
                } while (hosts[i] > value - 2);

                subnetsArr[i] = (uint)value;
            }

            // subnetArr 4,16,8,4,4,16 => subnetsCounts{ {4, 3}, {16, 2}, {8, 1} }
            var subnetsCounts = new Dictionary<uint, uint>();
            foreach (var subnet in subnetsArr)
            {
                if (subnetsCounts.ContainsKey(subnet)) subnetsCounts[subnet]++;
                else subnetsCounts.Add(subnet, 1u);
            }

            // хватит ли адресов на все подсети (считает адресное пространство, занимаемое подсетями и сравнивает тем, что есть)
            // [исходя из: каждая сеть имеет свой размер в x.Key, и таких подсетей x.Value => все занимаемые сетями адреса = сумма их произведений]
            var subnetsTotalReservedAddresses = subnetsCounts.Sum(x => x.Key * x.Value);
            if (network.AddressesCount <= subnetsTotalReservedAddresses)
                throw new SipCalcException(SipCalcExceptionCode.NotEnoughAddressesInNetwork);

            // subnetsCounts{ {4, 3}, {16, 2}, {8, 1} } => subnetRanges = 16, 8, 4
            var subnetsRanges = subnetsCounts.Keys.ToList();
            subnetsRanges.Sort((x, y) => -x.CompareTo(y)); // порядок сортировки важен: от большего размаха к меньшему

            var subnets = new List<NetworkIPv4Info>();
            var nextFreeAddress = network.NetworkAddress;

            foreach (var range in subnetsRanges)
            {
                var rangeMask = new Quatronima(~(range - 1));
                do
                {
                    var newSubnet = nextFreeAddress;
                    nextFreeAddress += range;
                    if (newSubnet >= network.BroadcastAddress) break; // по идее, не должно, если я не налажал с предыдущей проверкой
                    subnetsCounts[range]--;
                    subnets.Add(new NetworkIPv4Info(newSubnet, rangeMask));
                } while (subnetsCounts[range] > 0);

                // проверяет, каким образом вылетел из предыдущего цикла:
                // если остались нераспределённые сети текущего размера - значит не влезло
                if (subnetsCounts[range] > 0)
                    throw new SipCalcException(SipCalcExceptionCode.NotEnoughAddressesInNetwork);
            }

            return subnets;
        }
    }
}
