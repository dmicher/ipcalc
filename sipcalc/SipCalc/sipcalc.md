#### Remark befor start / Замечание перед началом
This manual is written in two languages: Russian and English. You can choose.
Данная инструкция составлена на двух языках: русском и английском. Можете выбирать.

---

# Englis
## The main functionality
The "Simple IP Calculator" module (sipcalc) allows you to get the basic parameters of an IPv4 network from its address and mask. The module also allows you to divide it into subnets according to one of the following principles:
- divide the network equally into the specified number of subnets;
- divide the network into subnets with variable size, specifying the sizes (in conneted hosts) of the subnets.

## Основные объекты пакета
### Quatronima
A "Quatronima" object is a positive 32-bit integer that can be represented as 4 single bytes separated by a dot and written in decimal, binary or hexadecimal notation.
With quatronima one can record a network, a host or a broadcast addresses, as well as mask and wildcard mask. 

There are several constructors for quatronima in the DLL. You can create them using:
- a 32-bit integer value;
- an array of 4 bytes, where the first byte is the highest;
- a string formated as "192.168.0.2" or "10101010.10101010.10101010.10101010";
- a prefix (for masks only).

Operators are defined for quatronimas:
- addition and subtraction between each other, as well as with positive integers (overflow is not allowed);
- bitwise operators "AND", "NOT" (for the application of masks and wildcard masks);
- comparison operations: equal to, not equal to, greater than, less than, not greater than, not less than.

Interfaces are defined for quatronima `IEquatable<Quatronima>` и `IComparable`.

Quatronima can be converted to:
- an array of bytes, 4 elements in size;
- a string of decimal, binary, and hexadecimal view;
- a prefix (for masks only).

_An additional property is provided for the Quatronima that reflects whether it can act as a mask or not_

### IPv4 Network (NetworkIPv4Info)
This object is a pair of quatronimas, one of which is a random address in the network, and the second one is the mask of this network. IPv4 network has a single constructor that requires to pass the address and mask to it. During initialization, it checks the mask for validity. After initial initialization there is no way to change the address or mask in this object.

Other network properties are calculated based on the address and mask.

For the network are defined:
- the address passed to the constructor, including the address itself, its network and terminal parts;
- mask passed to the constructor, including its prefix and wildcard mask;
- network address;
- the first network address available for host;
- the last network address available for host;
- broadcast address;
- total number of network addresses;
- the number of network addresses available for hosts.

_Also, the basic ToString () method has been overwritten for an IPv4 network object. It returns a short record of the network address and mask now._

### Calculator and exception objects created by it
The calculator object performs 3 basic operations:
- returns the IPv4 Network object by the specified address and mask (creates a new network object, checks for validity);
- splits the specified network into equal parts (this is useful when you need to divide the network equally by a known number of ports on the hardware);
- splits the specified network into parts with different sizes (this is useful when you need to divide the network into subnets with a known number of hosts).

You can get a new network using the IPv4 Network object constructor, or using the calculator method. The fundamental difference between these methods is that the Network object can throw a `System.ArgumentException`, while the calculator creates its own exceptions for these cases.

To split the network into equal parts, you should call the `Split Network` method and pass it the network that will be divided, as well as a positive integer, which means how many parts the network will be divided into. In General (if the transmitted number is not a power of two), the number of networks created may be greater than the requested number, but this number will not exceed the minimum required. For each subnet, the calculator will provide its basic data in the IPv4 Network object. The set of subnets will be returned as an enumeration sorted in ascending order of the subnet's network address.

To split a network into variable-length parts, call the `GetSubnetsByHosts` method and pass it the network that will be divided, as well as an array of integers. Each individual element of this array is a subnet, and the value of it is the number of hosts that need to be connected to this subnet. The calculator for each requested subnet detects the minimum sufficient network size and provides its basic data in an IPv4 object. The set of subnets will be provided as an enumeration, sorted in ascending order of the network address. Large networks will be located at the beginning of the address space, followed by smaller networks.

All objects returned by the calculator are IPv4 Networks (or their enumerations). Each network - a set of two quatronimas, for which the corresponding functionality described above.

It is also worth noting the auxiliary Exception objects `SipCalcException` and their codes `SipCalcExceptionCode` that the calculator can generate. The exception object itself `SipCalcException` inherited directly from `SystemException` and will contain an internal exception if necessary. In addition, this object has a field containing an exception code that is only relevant to the `SipCalc` calculator.

Possible exception codes:
- "No hosts in network" means that the network transferred for division does not contain any addresses that can be allocated for hosts, and therefore it is impossible to divide it into subnets;
- "No subnets required" - means that the method is passed a request to divide the network into 0 subnets of equal length, or an array of zero-length subnets of variable size;
- "Not enough addresses in network", means that the network contains not enough addresses for the network to be divided into the specified number of subnets, or into an array of subnets of variable length;
- "Can not initialize network" means that the network can't be created: details in the internal exception (most likely, the mask was not valid).

---

# Русский
## Основной функционал
Модуль "Simple IP Calculator" (sipcalc) позволяет получить основные параметры сети IPv4, зная его адрес и маску. Также модуль позволяет разделить эту сеть на подсети по одному из принципов:
- поровну поделить на указанное количество подсетей;
- разделить на подсети с переменным размером, указав размеры (в соединяемых терминалах) подсетей.

## Основные объекты пакета
### Кватронима (Quatronima)
Под объектом "Кватронима" понимается целое положительное 32-разрядное число, которое может быть представлено в виде 4 отдельных байтов, разделённых точкой, и записанных в десятчной, двоичной или шестнадцатиричной системе счисления.
Кватронимой может быть записаны адреса сети, терминалов, широковещательные, а также маска и обратная маска. 

Для кватроним определены конструкторы:
- по целому 32-разрядному значению;
- по массиву из 4 байтов, где первый байт - старший;
- по строке форматом "192.168.0.2" или "10101010.10101010.10101010.10101010";
- по префиксу (для масок).

Для кватроним определены операторы:
- сложения и вычетания между собой, а также с целыми положительными числами (переполенние не допускается);
- битовые операции "И", "НЕ" (для применения поямой и обратной масок);
- операции сравнения: равно, не равно, больше, меньше, не больше, не меньше.

Для кватроним определены интерфейсы `IEquatable<Quatronima>` и `IComparable`.

Кватронимы могут быть преобразованы:
- массив байтов, размером 4 элемента;
- в строку десятичного, двоичного и шестнадцатиричного представления;
- в префикс (только маски).

_дополнительно для кватроним пердусмотрено свойство, отражающее, может ли она выступать в качестве маски_

### Сеть IPv4 (NetworkIPv4Info)
Этот объект представляет собой пару кватроним, одна из которых - это произвольный адрес в сети, вторая - маска этой сети. Для сети IPv4 предусмотрен единственный конструктор, требующий передать ему адрес и маску. При инициализации проверяет маску на приемлемость. После первичной инициализации изменение адреса или маски в этом объекте не предусмотрено.

Другие свойства сети высчитываются на основе адреса и маски.

Для сети определяются:
- переданный конструктору адрес, в том числе сам адрес, его сетевая и терминальыне части;
- переданная конструктору маска, в том числе её префикс и обратная маска;
- адрес сети;
- первый адрес сети, доступный для назначения терминалу;
- последний адрес сети, доступный для назначения терминалу;
- широковещательный адрес;
- общее количество адресов сети;
- количество адресов сети, доступных для назначения терминалам.

_Также для объекта сети IPv4 переопределён базовый метод ToString(), который возвращает краткую запись сетевого адреса и маски сети._

### Калькулятор (Calculator) и объекты исключений, создаваемые им
Объект калькулятора производит 3 основные операции:
- возвращает объект Сеть IPv4 по заданным адресу и маске (создаёт новый объект сети, проверяет на приемлемость);
- разбивает указанную сеть на равные части (это будет удобно, когда нужно разделить поровну сеть по известному количеству портов на оборудовании);
- разбивает указанную сеть на части, размеры которых отличаются (это будет удобно, когда нужно разделить сеть на подсети, количество терминалов в которых известно).

Вы можете получить новую сеть, воспользовавшись конструктором объекта Сеть IPv4, либо методом калькулятора. Принципиальная разнича между этими методами заключена в том, что объект Сети может сгенерировать исключение System.ArgumentException, в то время как калькулятор создаёт собственные исключения на эти случаи.

Для разбиения сети на равные части вам следует вызвать метод [SplitNetwork] и передать ему сеть, которая будет разделена, а также целое положительное цисло, которое означает, на сколько частей будет поделена сеть. В общем случае (если передаваемое число не является степенью двойки), количество создаваемых сетей может оказаться больше запрошенного, но это количество не будет превышать минимально необходимое. По каждой подсети калькулятор предостави её основные данные в объекте Сеть IPv4. Набор подсетей будет возвращён в виде перечисления, отсортированного по позрастанию сетевого адреса подсети.

Для разбиения сети на части переменной длины следует вызвать метод [GetSubnetsByHosts] и передать ему сеть, которая будет разделена, а также массив целых чисел. Каждый отдельный элемент массива - это подсеть, а значение этого элемента - количество терминалов, которые нужно подключить к этой подсети. Калькулятор для каждой запрошенной подсети обнаружит минимально достаточный объём сети и предоставит её основные данные в объекте IPv4. Набор подсетей будет предоставлен в виде перечисления, отсортированными по возрастанию сетевого адреса. В начале адресного пространства будут располагаться большие сети, затем - сети меньшего размера.

Все объекты, возвращаемые калькулятором - Сеть IPv4 (или их перечисление). Каждая сеть - набор из двух кватроним, для которых доступен соответствующий функционал, описанный выше.

Здесь же стоит отметить вспомогательные объекты Исключений [SipCalcException] и их кодов [SipCalcExceptionCode], которые калькулятор может сгенерировать. Сам объект исключения [SipCalcException] унаследован напрямую от [SystemException] и, при необходимости, будет содежать внутреннее исключение. Кроме того, у этого объекта имеется поле, содержащее код исключения, имеющий отношение только к калькулятору [SipCalc].

Возможные коды исключений:
- "В сети нет терминалов", - означает, что переданная на деление сеть, в принципе, не содержит адресов, которые можно выделить под терминалы, и поэтому разделение на подсети невозможно;
- "Разделение на сети не требуется", - означает, что в метод передано требование разделить сеть на 0 подсетей равной длины, либо на массив нулевой длины подсетей переменного размера;
- "Не достаточно адресов в сети", - означает, что в сети содержится недостаточное количество адресов, чтобы сеть могла быть разделена на указанное количество подсетей, либо на массив подсетей переменной длины;
- "Не могу проинициализировать сеть", - означает, что сеть не может быть создана: подробности во внутреннем исключении (скорее всего, маска оказалась неприемлемой).
